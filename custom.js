//
var init_According_ITDev = function()
{
	try{
	jQuery(document).ready(function($) {
    // Find the elements with the class "elementor-accordion-icon-left"
    var elementsToReplace = $('.elementor-accordion-icon-left');

    // Replace the class with "elementor-accordion-icon-right"
    elementsToReplace.removeClass('elementor-accordion-icon-left').addClass('elementor-accordion-icon-right');
	});
	}
	catch(err)
	{
		
	}
	
	
	// replace icon 
	let p_icon_closed = '<svg class="e-font-icon-svg e-fas-plus" viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><path d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>'
	let p_icon_opened = '<svg class="e-font-icon-svg e-fas-plus" viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><path d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>'

	var p_accordion_icon_closed = document.getElementsByClassName('elementor-accordion-icon-closed');
	var p_accordion_icon_opened = document.getElementsByClassName('elementor-accordion-icon-opened');
	
	for(var i=0;i<p_accordion_icon_closed.length;i++)
	{
		p_accordion_icon_closed[i].innerHTML=p_icon_closed;
	}
	
	for(var i=0;i<p_accordion_icon_opened.length;i++)
	{
		p_accordion_icon_opened[i].innerHTML=p_icon_opened;
	}
	
	
	// Gen left num
	
	// get amount  elementor-accordion
	var p_elementor_accordion = document.getElementsByClassName('elementor-accordion');
	
	// work individual elementor-accordion
	for(var i=0;i<p_elementor_accordion.length;i++)
	{
		// work individual elementor-accordion-items
		var items=p_elementor_accordion[i].children;
		//console.dir(items);
		
		//console.log("TEST");
		for(var i2=0;i2<items.length;i2++)
		{
			//console.log("TEST");
			//console.dir(items[i]);
			var items_num = document.createElement("span");
			//items_num.classList.add("elementor-accordion-num");
			items_num.classList.add("elementor-accordion-num");
			if(i2<9){items_num.innerText = "0" + (i2+1);}
			else {items_num.innerText = (i2+1);}
			//var items_num= "<span class='.elementor-accordion-num'>00</span>";
			//items[i2].children[0].appendChild(items_num);
			
			
			//items[i2].appendChild(items_num);
			items[i2].insertBefore(items_num, items[i2].firstChild);
		}
		
	}
	
}


try{
document.addEventListener('DOMContentLoaded', init_According_ITDev(), false);
}
catch(err)
	{
			console.log(err);
	}
	
	
	
	//Keyboard triger
let currentIndexAcording = 0;
document.addEventListener('DOMContentLoaded', function() {
  const accordionItems = document.querySelectorAll('.elementor-accordion-item');

  // Add event listeners for keyboard navigation
  document.addEventListener('keydown', function(e) {
    if (e.key === 'ArrowUp') {
      currentIndexAcording = Math.max(currentIndexAcording - 1, 0);
    } else if (e.key === 'ArrowDown') {
      currentIndexAcording = Math.min(currentIndexAcording + 1, accordionItems.length - 1);
    }

    //console.dir(document.getElementsByClassName('elementor-tab-title'));
	document.getElementsByClassName('elementor-tab-title')[currentIndexAcording].click();
	//document.getElementsByClassName('elementor-tab-title')[currentIndexAcording].scrollIntoView({ behavior: 'smooth' });
  });
});
// Fix position after click mouse
jQuery(document).ready(function($) {
  $('.elementor-tab-title').on('click', function() {
    var id = $(this).attr('id');
    //console.log('Clicked element ID:', id);
	
	const accordionItems = document.querySelectorAll('.elementor-tab-title');
	for(var i=0;i<accordionItems.length;i++)
	{
		//console.log(accordionItems[i].id);
		//console.log(id);
		//console.log(accordionItems[i].id==id);
		if(accordionItems[i].id==id){currentIndexAcording=i;}
	}
  });
});
