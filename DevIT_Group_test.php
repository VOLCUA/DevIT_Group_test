<?php
/*
Plugin Name: DevIT_Group_test
Description: Додавання кастомних JavaScript та CSS файлів в хедер та футер.
Version: 1.0
Author: VOLCUA
*/


function my_custom_scripts() {
    // Підключення CSS
    wp_enqueue_style('DevIT_Group_test', plugin_dir_url(__FILE__) . 'style.css');
    
    // Підключення JS
    wp_enqueue_script('DevIT_Group_test', plugin_dir_url(__FILE__) . 'custom.js', array('jquery'), null, true);
}

add_action('wp_enqueue_scripts', 'my_custom_scripts');


function add_custom_css_to_header() {
    echo '<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">';
}

add_action('wp_head', 'add_custom_css_to_header');